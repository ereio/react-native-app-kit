import { StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get( 'window' );

// Style Imports
import colors from '/src/app/colors';

module.exports = StyleSheet.create( {
  // ROOT STYLE
  container: {
    width: width * 0.20,
    height: width * 0.20,
    borderRadius: width * 0.20,
    backgroundColor: colors.primary
  }
} );
