import React, { Component } from 'react';
import { Platform, Text, View } from 'react-native';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import dimens from '/src/app/dimens';
import colors from '/src/app/colors';

// Local Styling
import styles from './styles';

export default class CheckmarkCircle extends Component {
  render() {
    return ( <View style={styles.container}></View> );
  }
}
