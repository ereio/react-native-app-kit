import { Platform, StyleSheet, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create({
  container: {
    flex: 1
  },
  stepContainer: {
    flex: 1,
  },
});