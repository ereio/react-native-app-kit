import React, { Component } from 'react';
import { Animated, Platform, Text, View, Image } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as signupActions from 'src/app/domain/intro/signup/actions';

// Global Components
import MaterialMutliTextField from 'src/app/components/textfield/material';
import MaterialTextField from 'src/app/components/textfield/material';
import MaterialButton from 'src/app/components/button/material';

// Icons
import { VANILLA_ICON } from 'src/app/assets';

// Global Styling
import colors from 'src/app/colors';
import strings from 'src/app/strings';

// Local Styling
import styles from './styles';

class PersonalInfoPage extends Component {
  constructor(props) {
    super(props)

    this.onChangeFullname = this
      .onChangeFullname
      .bind(this);
    this.onChangeEmail = this
      .onChangeEmail
      .bind(this);
    this.onChangeAbout = this
      .onChangeAbout
      .bind(this);
    this.onEndEditingPersonalInfo = this
      .onEndEditingPersonalInfo
      .bind(this);
    this.onContinue = this.onContinue.bind(this);
  }

  componentDidMount() {
    this.onEndEditingPersonalInfo();
  }

  onChangeFullname(fullname) {
    const { setFullname } = this.props.actions;
    setFullname(fullname);
  }

  onChangeEmail(email) {
    const { setEmail } = this.props.actions;
    setEmail(email);
  }

  onChangeAbout(about) {
    const { setAbout } = this.props.actions;
    setAbout(about);
  }

  onEndEditingPersonalInfo() {
    const { checkPersonalInfo } = this.props.actions;
    const { fullname, email, about } = this.props.signup;
    checkPersonalInfo(fullname, email, about);
  }

  onContinue() {
    const { onContinue } = this.props;
    onContinue();
  }

  render() {
    const { fullname, email, about, isPersonalInfoAccepted } = this.props.signup;
    return(
      <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image source={VANILLA_ICON} style={styles.logoImage}/>
        <Text style={styles.signupTitleText}>
          {strings.SIGNUP_PERSONAL_INFO_TITLE}
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <MaterialTextField
          maxLength={48}
          returnKeyType={"next"}
          defaultValue={fullname}
          onChangeText={this.onChangeFullname}
          onEndEditing={this.onEndEditingPersonalInfo}
          placeholder={"Full Name"}/>
        <MaterialTextField
          maxLength={48}
          returnKeyType={"next"}
          defaultValue={email}
          onChangeText={this.onChangeEmail}
          onEndEditing={this.onEndEditingPersonalInfo}
          placeholder={"Email"}/>
        <MaterialMutliTextField
          maxLength={350}
          returnKeyType={"next"}
          defaultValue={about}
          onChangeText={this.onChangeAbout}
          onEndEditing={this.onEndEditingPersonalInfo}
          placeholder={"About You"}/>
      </View>
      <View style={styles.buttonContainer}>
        <MaterialButton
          text="Continue"
          onPress={this.onContinue}
          disabled={!isPersonalInfoAccepted}
          backgroundColor={isPersonalInfoAccepted
            ? colors.primary
            : colors.inactive}/>
      </View>
    </View>
    );
  }
}

const mapStateToProps = state => ({ signup: state.intro.signup });

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(signupActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfoPage);