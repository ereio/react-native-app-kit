import React, { Component } from 'react';
import { Animated, Platform, Text, View, Image } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as signupActions from 'src/app/domain/intro/signup/actions';

// Global Components
import MaterialMutliTextField from 'src/app/components/textfield/material';
import MaterialTextField from 'src/app/components/textfield/material';
import MaterialButton from 'src/app/components/button/material';

// Icons
import { APP_ICON } from 'src/app/assets';

// Global Styling
import colors from 'src/app/colors';
import strings from 'src/app/strings';

// Local Styling
import styles from './styles';

class BasicInfoPage extends Component {
  constructor(props) {
    super(props)

    // Why? otherwise creates a new function in memeory each time the component
    // renders
    // https://github.com/airbnb/javascript/tree/master/react#user-content-methods
    // TODO: break down pages into components
    this.onChangeUsername = this
      .onChangeUsername
      .bind(this);
    this.onChangePassword = this
      .onChangePassword
      .bind(this);
    this.onChangeConfirm = this
      .onChangeConfirm
      .bind(this);
    this.onEndEditingBasicInfo = this
      .onEndEditingBasicInfo
      .bind(this);
    this.onContinue = this.onContinue.bind(this);
  }

  onChangeUsername(username) {
    const { setUsername } = this.props.actions;
    setUsername(username);
  }

  onChangePassword(password) {
    const { setPassword } = this.props.actions;
    setPassword(password);
  }

  onChangeConfirm(confirm) {
    const { setConfirm } = this.props.actions;
    setConfirm(confirm);
  }

  onEndEditingBasicInfo() {
    const { checkBasicInfo } = this.props.actions;
    const { username, password, confirm } = this.props.signup;
    checkBasicInfo(username, password, confirm);
  }

  onContinue() {
    const { onContinue } = this.props;
    onContinue();
  }

  render() {
    const { username, password, confirm, isBasicInfoAccepted } = this.props.signup;
    return(
      <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image source={APP_ICON} style={styles.logoImage}/>
        <Text style={styles.signupTitleText}>
          {strings.SIGNUP_LOGIN_INFO_TITLE}
        </Text>
      </View>
      <View style={styles.inputContainer}>
        <MaterialTextField
          maxLength={32}
          returnKeyType={"next"}
          onChangeText={this.onChangeUsername}
          onEndEditing={this.onEndEditingBasicInfo}
          defaultValue={username}
          placeholder={"Username"}/>
        <MaterialTextField
          password={true}
          maxLength={48}
          returnKeyType={"next"}
          onChangeText={this.onChangePassword}
          onEndEditing={this.onEndEditingBasicInfo}
          defaultValue={password}
          placeholder={"Password"}/>
        <MaterialTextField
          password={true}
          maxLength={48}
          returnKeyType={"next"}
          defaultValue={confirm}
          onChangeText={this.onChangeConfirm}
          onEndEditing={this.onEndEditingBasicInfo}
          placeholder={"Confirm"}/>
      </View>
      <View style={styles.buttonContainer}>
        <MaterialButton
          text="Continue"
          onPress={this.onContinue}
          disabled={!isBasicInfoAccepted}
          backgroundColor={isBasicInfoAccepted
            ? colors.primary
            : colors.inactive}/>
      </View>
    </View>
    );
  }
}

const mapStateToProps = state => ({ signup: state.intro.signup });

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(signupActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(BasicInfoPage);