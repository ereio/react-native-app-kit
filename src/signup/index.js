import React, { Component } from 'react';
import { Animated, Platform, Dimensions, Easing, Text, View, Image } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as signupActions from 'src/app/domain/intro/signup/actions';

// Lib Components
import { IndicatorViewPager, PagerDotIndicator } from 'rn-viewpager';

// Global Components
import Loading from '../loading'; // TODO: FUCK HASTE

// Local Components
import BasicInfoPage from './basic-info-page';
import PersonalInfoPage from './personal-info-page';
import PhotoUploadPage from './photo-upload-page';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { APP_ICON, BACK_ARROW } from 'src/app/assets';

// Global Styling
import colors from 'src/app/colors';
import strings from 'src/app/strings';

// Local Styling
import styles from './styles';
const { height, width } = Dimensions.get('window');

const FIRST_PAGE_INDEX = 0;
const SECOND_PAGE_INDEX = 1;
const FINAL_PAGE_INDEX = 2;
const LOADING_PAGE_INDEX = 3;

class Signup extends Component {
  // Header Options - https://v1.reactnavigation.org/docs/stack-navigator.html#headerbackimage
  static navigationOptions = ({ navigation }) => {
    /* TODO: Upgrading to React Navigation v2 will allow you to create backButton components */
    const { params = {} } = navigation.state;
    if(params.isBackDisabled) {
      return {
        header: null,
      }
    } else {
      return {
        headerBackImage: BACK_ARROW,
        headerTintColor: colors.primary,
      };
    }
  };

  constructor(props) {
    super(props)

    this.state = {
      currentPage: 0,
      pageCount: 1,
    }

    this.onPressFirstStepContinue = this
      .onPressFirstStepContinue
      .bind(this);
    this.onPressSecondStepContinue = this
      .onPressSecondStepContinue
      .bind(this);
    this.onPressCompleteSignup = this
      .onPressCompleteSignup
      .bind(this);
    this.onPageScroll = this.onPageScroll.bind(this);
    this.onFinishedSignup = this.onFinishedSignup.bind(this);
  }

  // Initializes pages if exiting from signup and returning
  componentWillMount() {
    const { isBasicInfoAccepted, isPersonalInfoAccepted } = this.props.signup;
    if(isPersonalInfoAccepted) {
      this.setState({ pageCount: 3 })
    } else if(isBasicInfoAccepted) {
      this.setState({ pageCount: 2 })
    }
  }

  /*
    Updates the pages accessible by the ViewPager based on what you've completed
  */
  componentDidUpdate() {
    const { pageCount, currentPage } = this.state;
    const {
      isSignupProcessing,
      isSignupComplete,
      isBasicInfoAccepted,
      isPersonalInfoAccepted
    } = this.props.signup;

    if(isBasicInfoAccepted && pageCount < 2) {
      this.setState({ pageCount: 2 })
    } else if(isPersonalInfoAccepted && pageCount < 3) {
      this.setState({ pageCount: 3 })
    } else if((isSignupProcessing || isSignupComplete) && currentPage.position !==
      LOADING_PAGE_INDEX) {
      this.indicatorViewPager.setPage(LOADING_PAGE_INDEX);
    }
  }

  onPageScroll(page) {
    // TODO: get rid of this trash
    this.setState({ forceUpdate: !this.state.forceUpdate });
    this.setState({ currentPage: page });
  }

  onPressFirstStepContinue() {
    this
      .indicatorViewPager
      .setPage(SECOND_PAGE_INDEX);
  }

  onPressSecondStepContinue() {
    this
      .indicatorViewPager
      .setPage(FINAL_PAGE_INDEX);
  }

  onPressCompleteSignup() {
    const { createUserRequest } = this.props.actions;
    this.props.navigation.setParams({ isBackDisabled: true });
    this.setState({ pageCount: 4 });
    createUserRequest();
  }

  onFinishedSignup() {
    const { navigate } = this.props.navigation;
    const { clearSignupData } = this.props.actions;
    const { signup } = this.props.signup;
    clearSignupData()
    navigate("AppView");
  }

  renderDotIndicator() {
    return(
      <PagerDotIndicator
      dotStyle={{
        backgroundColor: colors.inactive
      }}
      selectedDotStyle={{
        backgroundColor: colors.primary
      }}
      pageCount={3}/>
    );
  }

  /*
  Requirement of rn-viewpage for each contained component to have the root
  element be a view. These are linked to the store individually so they can be
  moved around at will. The steps are not dependent on the information being
  entered :) because it sucks when they are.

  onPageScroll={(page) => this.myFunction(page)}

  also rn-pageviewer is 0 indexed
  */
  addSecondPage() {
    const { isBasicInfoAccepted } = this.props.signup;
    if(isBasicInfoAccepted && this.state.pageCount > 1) {
      return(
        <View style={styles.stepContainer}>
      <PersonalInfoPage onContinue={this.onPressSecondStepContinue} />
    </View>
      )
    }
  }

  addThirdPage() {
    const { isPersonalInfoAccepted } = this.props.signup;
    if(isPersonalInfoAccepted && this.state.pageCount > 2) {
      return(
        <View style={styles.stepContainer}>
          <PhotoUploadPage onContinue={this.onPressCompleteSignup} />
        </View>
      )
    }
  }

  addLoadPage() {
    const { isSignupProcessing, isSignupComplete } = this.props.signup;
    if(isSignupProcessing || isSignupComplete) {
      return(
        <View style={styles.stepContainer}>
            <Loading init={true} onFinished={this.onFinishedSignup} isLoadingComplete={isSignupComplete}/>
          </View>
      );
    }
  }

  render() {
    return(
      <IndicatorViewPager
      style={styles.container}
      ref={ref => this.indicatorViewPager = ref}
      currentPage={0}
      onPageScroll={this.onPageScroll}
      indicator={this.renderDotIndicator()}>
      <View style={{flex:1}}>
        <BasicInfoPage onContinue={this.onPressFirstStepContinue} />
      </View>
      {this.addSecondPage()}
      {this.addThirdPage()}
      {this.addLoadPage()}
    </IndicatorViewPager>
    );
  }
}

const mapStateToProps = state => ({ signup: state.intro.signup });

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(signupActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);