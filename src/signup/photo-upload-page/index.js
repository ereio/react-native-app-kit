import React, { Component } from 'react';
import { Animated, Platform, Text, View, Image, } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as signupActions from 'src/app/domain/intro/signup/actions';

// Global Components
import MaterialMutliTextField from 'src/app/components/textfield/material';
import MaterialTextField from 'src/app/components/textfield/material';
import MaterialButton from 'src/app/components/button/material';
import ImageSelector from 'src/app/components/image-selector';

// Icons
import { BLUEBERRY_ICON } from 'src/app/assets';

// Global Styling
import colors from 'src/app/colors';
import strings from 'src/app/strings';

// Local Styling
import styles from './styles';

class PhotoUploadPage extends Component {
  constructor(props) {
    super(props);

    this.onChangePhotoUri = this.onChangePhotoUri.bind(this);
    this.onContinue = this.onContinue.bind(this);
  }

  onChangePhotoUri(uri) {
    const { setPhotoSource } = this.props.actions;
    setPhotoSource(uri);
  }

  onContinue() {
    const { onContinue } = this.props;
    onContinue();
  }

  render() {
    const { photoUri, isSignupAcceptable } = this.props.signup;
    return(
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image source={BLUEBERRY_ICON} style={styles.logoImage} />
          <Text style={styles.signupTitleText}>
            {strings.SIGNUP_PHOTO_INFO_TITLE}
          </Text>
        </View>
        <View style={styles.inputContainer}>
          <ImageSelector onImageSelect={this.onChangePhotoUri} imageUri={photoUri}/>
        </View>
        <View style={styles.buttonContainer}>
          <MaterialButton
            text="Create Account"
            onPress={this.onContinue}
            disabled={!isSignupAcceptable}
            backgroundColor={isSignupAcceptable
              ? colors.primary
              : colors.inactive} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  signup: state.intro.signup
});

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(signupActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PhotoUploadPage);