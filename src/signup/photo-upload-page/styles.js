import { Platform, StyleSheet, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerContainer: {
    flex: 3,
    flexDirection: "column",
    justifyContent: 'space-around',
    alignItems: "center",
    width: width * 0.75,
  },
  inputContainer: {
    flex: 2,
    flexDirection: "row",
    justifyContent: 'space-evenly',
    alignItems: "center",
    marginTop: width * 0.04,
    marginBottom: width * 0.16,
    marginHorizontal: width * 0.16,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "column",
    width: width * 0.75
  },
  logoImage: {
    marginTop: height * 0.04,
    width: width * 0.25,
    height: width * 0.25
  },
  signupTitleText: {
    fontSize: 24,
    textAlign: "center"
  }
});