import React, { Component } from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Platform,
  Text,
  View,
  Image
} from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// Actions
import * as authActions from 'src/app/domain/intro/auth/actions';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { APP_ICON, BACK_ARROW } from 'src/app/assets';

// Global Components
import MaterialTextField from 'src/app/components/textfield/material';
import MaterialButton from 'src/app/components/button/material';

// Global Styling
import colors from 'src/app/colors';
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';

class Login extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerBackImage: BACK_ARROW,
      headerTintColor: colors.primary,
    };
  };

  constructor(props) {
    super(props);
    this.onPressLogin = this.onPressLogin.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
  }

  componentWillMount() {
    const { clearLoginStatus } = this.props.actions;
    clearLoginStatus() // HACK: remove when logout works & session token works
  }

  componentDidUpdate() {
    const { status, loggedin } = this.props;
    const { navigate } = this.props.navigation;
    if(status === "ACTIVE" && loggedin) {
      navigate('AppView')
    }
  }

  onChangeUsername(username) {
    const { setLoginUsername } = this.props.actions;
    setLoginUsername(username);
  }

  onChangePassword(password) {
    const { setLoginPassword } = this.props.actions;
    setLoginPassword(password);
  }

  onPressLogin() {
    const { attemptAllLogins } = this.props.actions;
    const { navigate } = this.props.navigation;
    attemptAllLogins();
  }

  render() {
    const { username, password, loading } = this.props
    return(
      <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.loginTitleText}>
          React Native
        </Text>
        <Text style={styles.loginSubtitleText}>
          Starter Kit
        </Text>
        <Image source={APP_ICON} style={styles.logoImage}/>
      </View>
      <View style={styles.inputContainer}>
        <MaterialTextField placeholder={"username"} onChangeText={this.onChangeUsername} defaultValue={username} />
        <MaterialTextField password={true} placeholder={"password"} onChangeText={this.onChangePassword} value={password}/>
      </View>
      <View style={styles.buttonContainer}>
        <MaterialButton
          text="Login"
          disabled={loading}
          backgroundColor={loading
            ? colors.inactive
            : colors.primary}
            onPress={this.onPressLogin}/>
      </View>
    </View>
    );
  }
}
const mapStateToProps = state => ({ ...state.intro.auth });

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(authActions, dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);