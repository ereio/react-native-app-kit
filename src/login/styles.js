import { Platform, StyleSheet, Dimensions } from 'react-native';
const { height, width } = Dimensions.get( 'window' );

module.exports = StyleSheet.create( {
  container: {
    flex: 1,
    alignItems: 'center'
  },
  headerContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputContainer: {
    flex: 3,
    flexDirection: "column",
    justifyContent: 'center',
    width: width * 0.75
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "column",
    width: width * 0.72
  },
  logoImage: {
    marginTop: height * 0.04,
    width: width * 0.25,
    height: width * 0.25
  },
  loginTitleText: {
    fontSize: 32
  },
  loginSubtitleText: {
    fontSize: 24
  }
} );
