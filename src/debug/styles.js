import { Platform, StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';
import dimens from 'src/app/dimens';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: width * 0.18,
    paddingBottom: width * dimens.DEFAULT_RATIO_SMALL,
  },
  scrollViewContainer: {
    flex: 1,
  },
  cardContainer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    justifyContent: 'flex-start',
    marginHorizontal: width * dimens.DEFAULT_RATIO_SMALL,
    paddingVertical: width * dimens.DEFAULT_RATIO,
    paddingHorizontal: width * dimens.DEFAULT_RATIO_LARGE,
  },
  headerContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  photoContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginLeft: -(width * dimens.DEFAULT_RATIO_SMALL), // HACK: because materialicon has inherent padding
  },
  headerInfoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  statContainer: {
    flex: 2,
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },
  personalInfoContainer: {
    flex: 5,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  identificationContainer: {
    flex: 1,
  },
  aboutContainer: {
    flex: 2,
  },
  actionsContainer: {
    flex: 1,
  },
  defaultPhotoCircle: {
    color: colors.textSecondary,
  },
  photoCircle: {
    width: width * dimens.profileImageRatio,
    height: width * dimens.profileImageRatio,
    borderRadius: 100,
  },
  primaryText: {
    fontSize: 28,
    textAlign: 'center',
    color: colors.textSecondary,
  },
  subheaderText: {
    fontSize: 12,
    textAlign: 'right',
    color: colors.textSecondaryDark,
  },
  primaryInfoText: {
    fontSize: 32,
    textAlign: 'left',
    color: colors.textSecondary,
  },
  secondaryInfoText: {
    fontSize: 16,
    textAlign: 'left',
    color: colors.textSecondaryDark,
  },
  primaryInfoTextSmall: {
    fontSize: 18,
    textAlign: 'left',
    color: colors.textSecondary,
  },
});