import React, { Component } from 'react';
import { Platform, Text, View } from 'react-native';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\nCmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\nShake or press menu button for dev m' +
    'enu'
});

export default class Secondary extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `Secondary`,
      headerTitle: `Secondary`,
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcon
        name='donut-small'
        size={dimens.tabIcon}
        style={{
          color: tintColor
        }}/>
      )
    };
  };
  render() {
    return(
      <View style={styles.container}>
      <Text style={styles.welcome}>
        This is the Secondary Tab
      </Text>
      <Text style={styles.instructions}>
        {instructions}
      </Text>
    </View>
    );
  }
}