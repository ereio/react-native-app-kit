import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  Animated,
  Dimensions,
  Easing,
  View,
  Image
} from 'react-native';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { APP_ICON } from 'src/app/assets';

// Global Styling
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';

let { height, width } = Dimensions.get('window');

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scaleXY: new Animated.Value(width * 0.25),
      fading: false
    }
    this.checkAppState = this.checkAppState.bind(this);
  }

  checkAppState() {
    const { navigate } = this.props.navigation;
    const { loggedin } = this.props.auth;
    // TODO: Removed auto login until i fix the logout feature
    if(loggedin) {
      navigate('IntroViews')
      // navigate('AppView')
    } else {
      navigate('IntroViews')
    }
  }

  renderFading(fade) {
    if(fade) {
      Animated
        .timing(this.state.scaleXY, {
          toValue: -20,
          easing: Easing.back(),
          duration: 625
        })
        .start(this.checkAppState);
    }
  }

  render() {
    this.renderFading(true);
    return(
      <View style={styles.container}>
      <Animated.View
        style={{
          height: this.state.scaleXY,
          width: this.state.scaleXY
        }}>
        <Animated.Image
          style={{
            height: this.state.scaleXY,
            width: this.state.scaleXY,
            resizeMode: 'contain'
          }}
          source={APP_ICON}/>
      </Animated.View>
    </View>
    );
  }
}

const mapStateToProps = state => ({ auth: state.intro.auth });

export default connect(mapStateToProps)(Splash);