import { Platform, StyleSheet, Dimensions } from 'react-native';
let { height, width } = Dimensions.get( 'window' );

module.exports = StyleSheet.create( {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  splashIcon: {
    height: width * 0.25,
    width: width * 0.25
  }
} );
