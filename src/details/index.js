import React, { Component } from 'react';
import { Platform, Text, View } from 'react-native';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';

export default class Details extends Component {
  render() {
    return(
      <View style={styles.container}>
      <Text style={styles.welcome}>
        This is the Details View
      </Text>
      <Text style={styles.instructions}>
        {instructions}
      </Text>
    </View>
    );
  }
}