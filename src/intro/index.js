import React, { Component } from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Platform,
  Text,
  View,
  Image
} from 'react-native';

// Global Components
import MaterialTextField from 'src/app/components/textfield/material';
import MaterialButton from 'src/app/components/button/material';
import TextButton from 'src/app/components/textbutton/';

// Global Data
import strings from 'src/app/strings';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { APP_ICON } from 'src/app/assets';

// Local Styling
import styles from './styles';

export default class Intro extends Component {
  constructor(props) {
    super(props);
    this.onPressNavSignup = this
      .onPressNavSignup
      .bind(this);
    this.onPressNavLogin = this
      .onPressNavLogin
      .bind(this);
  }

  onPressNavSignup() {
    const { navigate, isFocused } = this.props.navigation;
    if(isFocused()) {
      navigate('Signup')
    }
  }

  onPressNavLogin() {
    const { navigate, isFocused } = this.props.navigation;
    if(isFocused()) {
      navigate('Login')
    }
  }

  render() {
    return(
      <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={styles.titleTextContainer}>
          <Text style={styles.introTitleText}>
            React Native
          </Text>
          <Text style={styles.introSubtitleText}>
            Starter Kit
          </Text>
        </View>
        <Image source={APP_ICON} style={styles.logoImage}/>
      </View>
      <View style={styles.infoContainer}>
        <Text style={styles.introInfoPrimaryText}>
          {strings.SIGNUP_INFO_PRIMARY}
        </Text>
        <Text style={styles.introInfoSecondaryText}>
          {strings.SIGNUP_INFO_SECONDARY}
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <View style={styles.buttonContainer}>
          <MaterialButton text="Signup"  onPress={this.onPressNavSignup}/>
        </View>
        <View style={styles.dividerContainer}>
          <View style={styles.divider}/>
        </View>
        <View style={styles.buttonContainer}>
          <TextButton
            infoText={"Already have an account?"}
            actionText={"Login"}
            onPress={this.onPressNavLogin}/>
        </View>
      </View>
    </View>
    );
  }
}