import { Platform, StyleSheet, Dimensions } from 'react-native';
const { height, width } = Dimensions.get( 'window' );

// Style Imports
import colors from 'src/app/colors';

/*
   Using Flexboxs allow the screen to adjust natively without having to directly assess height and
   width each time. Below the root container (called container) assumes the flex of 1. Since the resulting
   compontent has no siblings, the component fills the space within it's parent.

   The following containers assume different amounts of space. Out of the total number of flex between them,
   consider the ratio/percentage of space they maintain. If you give global components, such as a MaterialButton,
   a flex of one, they will be styled the same, but sized based on the amount of flex you give to it's container
   this allows you to use one global button component for different sized implementations.
*/
module.exports = StyleSheet.create( {
  container: {
    flex: 14
  },
  headerContainer: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    flex: 5,
    flexDirection: "column",
    justifyContent: 'center',
    marginHorizontal: width * 0.08
  },
  actionContainer: {
    flex: 4,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleTextContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: width * 0.08
  },
  dividerContainer: {
    flex: 1,
    flexDirection: "row",
    marginHorizontal: width * 0.12
  },
  divider: {
    flex: 1,
    marginVertical: height * 0.04,
    borderBottomColor: colors.textInactive,
    borderBottomWidth: 1
  },
  logoImage: {
    marginTop: 8,
    width: width * 0.25,
    height: width * 0.25
  },
  introTitleText: {
    fontSize: 32
  },
  introSubtitleText: {
    fontSize: 24
  },
  introInfoPrimaryText: {
    fontSize: 20,
    marginBottom: 5,
    textAlign: "center"
  },
  introInfoSecondaryText: {
    fontSize: 16,
    textAlign: "center"
  }
} );
