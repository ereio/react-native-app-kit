// Image Scaling - https://facebook.github.io/react-native/docs/images.html
// @2x @3x titled images will auto scale based on pixel density
module.exports = {
  APP_ICON: require("src/app/assets/starter-icon.png"),
  VANILLA_ICON: require("src/app/assets/starter-icon-vanilla.png"),
  BLUEBERRY_ICON: require("src/app/assets/starter-icon-blueberry.png"),
  STRAWBERRY_CONE_ICON: require("src/app/assets/strawberry-cone.png"),
  BLUEBERRY_CONE_ICON: require("src/app/assets/blueberry-cone.png"),
  VANILLA_CONE_ICON: require("src/app/assets/vanilla-cone.png"),
  BACK_ARROW: require("src/app/assets/baseline_arrow_back_ios_black_48dp.png"),
}