module.exports = {
  APP_VIEW: 'AppView',
  TAB_VIEW: 'TabView',
  PRIMARY: 'Primary',
  SECONDARY: 'Secondary',
  AUXILIARY: 'Auxiliary',
  PROFILE: 'Profile',
  DETAILS: 'Details',
  INTRO_VIEW: 'IntroView',
  INTRO: 'Intro',
  LOGIN: 'Login',
  SIGNUP: 'Signup',
  SPLASH: 'Splash',
};