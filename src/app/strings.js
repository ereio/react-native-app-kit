module.exports = {
  STARTER_INTRO: "Welcome to this react native starter kit",
  AUXILIARY_TITLE: "Auxilary",
  PRIMARY_TITLE: "Primary",
  SECONDARY_TITLE: "Secondary",
  PROFILE_TITLE: "Profile",
  SIGNUP_INFO_PRIMARY: "Easily create apps for ideas you love",
  SIGNUP_INFO_SECONDARY: "By using this kit, you already have many examples of UX design and the basics on implementing common features",
  SIGNUP_LOGIN_INFO_TITLE: "Create your user account",
  SIGNUP_PERSONAL_INFO_TITLE: "Tell us about you",
  SIGNUP_PHOTO_INFO_TITLE: "Add a photo if you want :)",
};