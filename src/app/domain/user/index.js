import profile from './profile/reducer';
import collectables from './collectables/reducer';

export {
  profile,
  collectables,
};