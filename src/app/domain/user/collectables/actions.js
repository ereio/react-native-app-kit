export const REMOVE_CONE_ATTEMPT = 'REMOVE_CONE_ATTEMPT';
export const REMOVE_CONE_SUCCESS = 'REMOVE_CONE_SUCCESS';

export const ADD_CONE_ATTEMPT = 'ADD_CONE_ATTEMPT';
export const ADD_CONE_SUCCESS = 'ADD_CONE_SUCCESS';
export const INCREMENT_COUNT = 'INCREMENT_COUNT';

export const CLEAR_COLLECTION_DATA = 'CLEAR_COLLECTION_DATA';

export const addCone = (cone) => {
  return {
    type: ADD_CONE_SUCCESS,
    user: cone,
  }
}

export const removeCone = (cone) => {
  return {
    type: REMOVE_CONE_SUCCESS,
    user: cone,
  }
}

export const incrementCount = () => {
  return (dispatch) => {
    dispatch({
      type: INCREMENT_COUNT,
    });
  }
}

export const attemptAddCone = (cone) => {
  return (dispatch, getState) => {
    dispatch({
      type: ADD_CONE_ATTEMPT
    });

    dispatch(addCone(cone));
  }
}

export const attemptRemoveCone = (cone) => {
  return (dispatch, getState) => {
    dispatch({
      type: REMOVE_CONE_ATTEMPT
    });

    dispatch(removeCone(cone));
  }
}