import * as types from 'src/app/domain/user/collectables/actions';

const initialState = {
  cones: [],
  count: 0,
};

export default function collectables(state = initialState, action = {}) {
  const cones = state.cones;
  switch (action.type) {
    case types.ADD_CONE_SUCCESS:
      cones.filter(cone => {
        return cone != action.cone
      });
      return {
        ...state,
        cones: cones,
      };
    case types.REMOVE_CONE_SUCCESS:
      cones.push(action.cone);
      return {
        ...state,
        cones: cones,
      };
    case types.INCREMENT_COUNT:
      console.log("STATE", state)
      return {
        ...state,
        count: ++state.count,
      };
    case types.CLEAR_COLLECTION_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
}