import * as types from 'src/app/domain/user/profile/actions';

const initialState = {
  id: "",
  hash: "",
  username: "",
  fullname: "",
  email: "",
  about: "",
  photoUri: "",
};

export default function profile(state = initialState, action = {}) {
  switch (action.type) {
    case types.LOAD_PROFILE_ATTEMPT:
      return {
        ...state,
      };
    case types.LOAD_PROFILE_SUCCESS:
      return {
        ...state,
        id: 1,
        hash: action.profile.hash || null,
        username: action.profile.username || null,
        fullname: action.profile.fullname || null,
        email: action.profile.email || null,
        about: action.profile.about || null,
        photoUri: action.profile.photoUri || null,
      };
    case types.CLEAR_PROFILE_DATA:
      return {
        ...initialState,
      }
    default:
      return state;
  }
}