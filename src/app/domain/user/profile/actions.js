export const LOAD_PROFILE_SUCCESS = 'LOAD_PROFILE_SUCCESS';
export const LOAD_PROFILE_ATTEMPT = 'LOAD_PROFILE_ATTEMPT';
export const UPDATE_PROFILE_ATTEMPT = 'UPDATE_PROFILE_ATTEMPT';
export const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
export const CLEAR_PROFILE_DATA = 'CLEAR_PROFILE_DATA';

export const SET_USERNAME = 'SET_USERNAME';
export const SET_PASSWORD = 'SET_PASSWORD';

export const setProfileData = () => {
  return (dispatch, getState) => {
    const { signup } = getState().intro
    dispatch({
      type: LOAD_PROFILE_SUCCESS,
      profile: signup,
    });
  }
}

export const fetchProfileData = (status) => {
  return (dispatch, getState) => {
    const { profile } = getState().user;
    dispatch({
      type: LOAD_PROFILE_ATTEMPT
    });

    dispatch(setProfileData(profile));
  }
}

export const updateProfileData = (type, data) => {
  return (dispatch, getState) => {
    dispatch({
      type: UPDATE_PROFILE_ATTEMPT
    });

    dispatch({
      type: UPDATE_PROFILE_SUCCESS,
    });
  }
}

export const clearProfileData = () => {
  return (dispatch, getState) => {
    dispatch({
      type: CLEAR_PROFILE_DATA
    });
  }
}