import * as types from 'src/app/domain/intro/auth/actions';

const initialState = {
  loggedin: false,
  status: null,
  loading: false,
  username: '',
  password: '',
  hashs: [],
};

export default function auth(state = initialState, action = {}) {
  console.log("TYPE | ", action.type, action);
  switch (action.type) {
    case types.SET_HASH:
      const hashs = [...state.hashs, action.hash];
      return {
        ...state,
        hashs: hashs
      }
    case types.SET_LOGIN_USERNAME:
      return {
        ...state,
        loading: false,
        username: action.username
      };
    case types.SET_LOGIN_PASSWORD:
      return {
        ...state,
        loading: false,
        password: action.password
      };
    case types.LOGIN_ATTEMPT:
      return {
        ...state,
        loading: true,
        loggedin: false,
        status: "INACTIVE"
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        password: '',
        loggedin: false,
        status: "INACTIVE"
      };
    case types.LOGIN_SUCCESS:
      return {
        ...initialState,
        hashs: state.hashs,
        loading: false,
        loggedin: true,
        status: "ACTIVE"
      };
    case types.LOGIN_SUCCESS_STRICT:
      return {
        ...initialState,
        hashs: state.hashs,
        loading: false,
        loggedin: false,
        status: "ACTIVE"
      };
    case types.LOGOUT_SUCCESS:
      return {
        ...initialState,
        hashs: state.hashs,
        loading: false,
        loggedin: false,
        status: "INACTIVE"
      };
    case types.CLEAR_AUTH_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
}