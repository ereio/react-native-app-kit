import bcrypt from 'react-native-bcrypt';

import { accessUserEncryptedStore, lockUserEncryptedStore } from 'src/app/store';

export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_SUCCESS_STRICT = 'LOGIN_SUCCESS_STRICT';

export const LOGOUT_ATTEMPT = 'LOGOUT_ATTEMPT';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const SET_LOGIN_USERNAME = 'SET_LOGIN_USERNAME';
export const SET_LOGIN_PASSWORD = 'SET_LOGIN_PASSWORD';

export const CLEAR_AUTH_DATA = 'CLEAR_AUTH_DATA';
export const SET_HASH = 'SET_HASH';

export const setLoginUsername = (username) => {
  return (dispatch) => {
    dispatch({
      type: SET_LOGIN_USERNAME,
      username: username
    });
  }
}

export const setLoginPassword = (password) => {
  return (dispatch) => {
    dispatch({
      type: SET_LOGIN_PASSWORD,
      password: password
    });
  }
}

export const clearAuthData = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_AUTH_DATA
    })
  }
}
export const clearLoginStatus = () => {
  return (dispatch) => {
    dispatch({
      type: LOGOUT_SUCCESS
    })
  }
}
// TODO: i think there's a more secure way of doing this without
// any involvment of the original password.
//
// if the hashed password matches, it will unlock the store
// using the original password as the secret key. We need the hash to be publically available
// and only testable against a live password given by the user
export const attemptAllLogins = (isStrict) => {
  return (dispatch, getState) => {
    const { hashs, username, password } = getState().intro.auth;

    const attempts = hashs.map((hash) => {
      return bcrypt.compareSync(password, hash);
    });

    if (attempts.every((attempt) => !attempt)) {
      dispatch({
        type: LOGIN_FAILURE,
      });
    } else {
      if (isStrict) {
        dispatch({
          type: LOGIN_SUCCESS_STRICT,
        });
      } else {
        dispatch({
          type: LOGIN_SUCCESS,
        });
      }
      accessUserEncryptedStore(username, password);
    }
  }
}


export const attemptLogin = () => {
  // TODO: implement login without checking all hashes
}

export const attemptLogout = () => {
  return (dispatch, getState) => {
    dispatch({
      type: LOGOUT_ATTEMPT
    });

    lockUserEncryptedStore();

    dispatch({
      type: LOGOUT_SUCCESS,
    });
  }
}