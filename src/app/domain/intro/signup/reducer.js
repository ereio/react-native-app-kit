import * as types from 'src/app/domain/intro/signup/actions';

const initialState = {
  isBasicInfoAccepted: false,
  isPersonalInfoAccepted: false,
  isSignupAcceptable: false,
  isSignupProcessing: false,
  isSignupComplete: false,
  username: "",
  password: "",
  confirm: "",
  hash: "",
  fullname: "",
  email: "",
  about: "",
  photoUri: "",
};

export default function signup(state = initialState, action = {}) {
  switch (action.type) {
    case types.CLEAR_SIGNUP_DATA:
      return {
        ...initialState
      };
    case types.SET_USERNAME:
      return {
        ...state,
        username: action.username
      };
    case types.SET_HASH:
      return {
        ...state,
        hash: action.hash
      };
    case types.SET_PASSWORD:
      return {
        ...state,
        password: action.password
      };
    case types.SET_CONFIRM:
      return {
        ...state,
        confirm: action.confirm
      };
    case types.SET_FULLNAME:
      return {
        ...state,
        fullname: action.fullname
      };
    case types.SET_EMAIL:
      return {
        ...state,
        email: action.email
      };
    case types.SET_ABOUT:
      return {
        ...state,
        about: action.about
      };
    case types.SET_PHOTOURL:
      return {
        ...state,
        photoUri: action.photoUri
      };
    case types.CHECK_BASIC_INFO:
      return {
        ...state,
      };
    case types.ACCEPT_BASIC_INFO:
      return {
        ...state,
        isBasicInfoAccepted: true,
        isSignupAcceptable: true && state.isPersonalInfoAccepted
      };
    case types.DENY_BASIC_INFO:
      return {
        ...state,
        isBasicInfoAccepted: false,
        isSignupAcceptable: false && state.isPersonalInfoAccepted
      };
    case types.ACCEPT_PERSONAL_INFO:
      return {
        ...state,
        isPersonalInfoAccepted: true,
        isSignupAcceptable: state.isBasicInfoAccepted && true
      };
    case types.DENY_PERSONAL_INFO:
      return {
        ...state,
        isPersonalInfoAccepted: false,
        isSignupAcceptable: state.isBasicInfoAccepted && false
      };
    case types.CREATE_ACCOUNT_ATTEMPT:
      return {
        ...state,
        isSignupProcessing: true
      };
    case types.CREATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        isBasicInfoAccepted: false,
        isPersonalInfoAccepted: false,
        isSignupAcceptable: false,
        isSignupComplete: true
      };
    default:
      return state;
  }
}