import bcrypt from 'react-native-bcrypt';
import seedrandom from 'seedrandom';

import * as profileActions from 'src/app/domain/user/profile/actions';
import * as authActions from 'src/app/domain/intro/auth/actions';

export const SET_USERNAME = 'SET_USERNAME';
export const SET_PASSWORD = 'SET_PASSWORD';
export const SET_CONFIRM = 'SET_CONFIRM';
export const SET_FULLNAME = 'SET_FULLNAME';
export const SET_EMAIL = 'SET_EMAIL';
export const SET_ABOUT = 'SET_ABOUT';
export const SET_PHOTOURL = 'SET_PHOTOURL';
export const SET_HASH = 'SET_HASH';

export const CHECK_BASIC_INFO = 'CHECK_BASIC_INFO';
export const ACCEPT_BASIC_INFO = 'ACCEPT_BASIC_INFO';
export const DENY_BASIC_INFO = 'DENY_BASIC_INFO';

export const CHECK_PERSONAL_INFO = 'CHECK_PERSONAL_INFO';
export const ACCEPT_PERSONAL_INFO = 'ACCEPT_PERSONAL_INFO';
export const DENY_PERSONAL_INFO = 'DENY_PERSONAL_INFO';

export const CREATE_ACCOUNT_ATTEMPT = 'CREATE_ACCOUNT_ATTEMPT';
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS';
export const CREATE_ACCOUNT_FAILURE = 'CREATE_ACCOUNT_FAILURE';

export const CLEAR_SIGNUP_DATA = 'CLEAR_SIGNUP_DATA';

const SALT_ROUNDS = 7;

export const setUsername = (username) => {
  return (dispatch) => {
    dispatch({
      type: SET_USERNAME,
      username: username
    });
  }
}

export const setPassword = (password) => {
  return (dispatch) => {
    dispatch({
      type: SET_PASSWORD,
      password: password
    });
  }
}

export const setConfirm = (confirm) => {
  return (dispatch) => {
    dispatch({
      type: SET_CONFIRM,
      confirm: confirm
    });
  }
}


export const setFullname = (fullname) => {
  return (dispatch) => {
    dispatch({
      type: SET_FULLNAME,
      fullname: fullname
    });
  }
}

export const setEmail = (email) => {
  return (dispatch) => {
    dispatch({
      type: SET_EMAIL,
      email: email
    });
  }
}
export const setAbout = (about) => {
  return (dispatch) => {
    dispatch({
      type: SET_ABOUT,
      about: about
    });
  }
}

export const setPhotoUri = (photoUri) => {
  return (dispatch) => {
    dispatch({
      type: SET_PHOTOURL,
      photoUri: photoUri
    });
  }
}

export const setHash = (hash) => {
  return {
    type: SET_HASH,
    hash: hash
  }
}

export const setPhotoSource = (uri) => {
  return (dispatch, getState) => {
    dispatch(setPhotoUri(uri));
  }
}

export const checkBasicInfo = (username, password, confirm) => {
  return (dispatch, getState) => {
    let invalid = false;
    dispatch({
      type: CHECK_BASIC_INFO
    });

    if (username.length < 3) {
      invalid = true;
    }
    if (password.length < 8) {
      invalid = true;
    }
    if (password !== confirm) {
      invalid = true;
    }

    dispatch({
      type: invalid ? DENY_BASIC_INFO : ACCEPT_BASIC_INFO,
    });
  }
}

export const checkPersonalInfo = (fullname, email, about) => {
  return (dispatch, getState) => {
    let invalid = false;
    dispatch({
      type: CHECK_PERSONAL_INFO
    });

    if (fullname.length < 3) {
      invalid = true;
    }
    if (email.length < 4) {
      invalid = true;
    }

    dispatch({
      type: invalid ? DENY_PERSONAL_INFO : ACCEPT_PERSONAL_INFO,
    });
  }
}

export const clearSignupData = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_SIGNUP_DATA,
    })
  }
}

export const createUser = () => {
  return (dispatch, getState) => {
    const {
      setLoginUsername,
      setLoginPassword,
      attemptAllLogins,
    } = authActions;
    const { signup } = getState().intro;
    const { setProfileData } = profileActions
    // Set account data to user data
    dispatch(setLoginUsername(signup.username))
    dispatch(setLoginPassword(signup.password))
    dispatch(attemptAllLogins(true));
    dispatch(setProfileData());
  }
}


export const createUserRequest = () => {
  return (dispatch, getState) => {
    const { signup } = getState().intro;

    dispatch({
      type: CREATE_ACCOUNT_ATTEMPT,
    });

    /*
    If successfully created this wipe the signup data and set
    it to the current user
    (it always is because there's no real service calls here)
    Using seedrandom to generate a PRNG with ARC4
    */
    const plaintextPassword = signup.password;
    const rng = seedrandom();

    // HACK to allow for Uint8Array handling in Android
    // Android defined Uint8Arrays as a joined object
    bcrypt.setRandomFallback((len) => {
      const preBuff = new Uint8Array(len);
      return [...preBuff].map(() => Math.floor(rng() * 256))
    });

    bcrypt.genSalt(SALT_ROUNDS, (error, salt) => {
      if (error) {
        console.log("ERROR genSalt", error);
        dispatch({
          type: CREATE_ACCOUNT_FAILURE,
        });
      } else {
        bcrypt.hash(plaintextPassword, salt, (error, hash) => {
          // Store hash in your password DB.
          if (error) {
            console.log("ERROR hash", error);
            dispatch({
              type: CREATE_ACCOUNT_FAILURE,
            });
          } else {
            dispatch(setHash(hash));
            dispatch(createUser());
            dispatch({
              type: CREATE_ACCOUNT_SUCCESS,
            });
          }
        });
      }
    });
  }
}