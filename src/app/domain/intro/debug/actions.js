export const DEBUG_ACTION_FAILURE = 'DEBUG_ACTION_FAILURE';
export const DEBUG_ACTION_SUCCESS = 'DEBUG_ACTION_SUCCESS';

export const setDebug = (someValue) => {
  return (dispatch) => {
    dispatch({
      type: DEBUG_ACTION_SUCCESS,
      someValue: someValue
    });
  }
}