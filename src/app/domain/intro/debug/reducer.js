import * as types from 'src/app/domain/intro/signup/actions';

const initialState = {
  id: "1029s0fk1o23u0a8su0ij;1l2k3j09asu9j1k2mn3p9ais0d9i",
  hash: "p1j2039as0dijk12p039ua09uc98ajsodnl1k2m3po1je0sa9u98jaosijd",
  username: "taylorereio",
  fullname: "Taylor Ereio",
  email: "ereio@protonmail.com",
  about: "Hey, my name is taylor. I love helping people and spending wayy too much time helping those who don't want to be helped. i often try too hard to a fault and then i sometimes make things worse. trying to focus.",
  photoUri: "",
  cones: [],
  count: 0,
};

export default function debug(state = initialState, action = {}) {
  switch (action.type) {
    case types.DEBUG_ACTION_FAILURE:
      return {
        ...initialState
      };
    case types.DEBUG_ACTION_SUCCESS:
      return {
        ...state,
        someValue: action.someValue
      };
    default:
      return state;
  }
}