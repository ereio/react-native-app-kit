import auth from './auth/reducer';
import signup from './signup/reducer';
import debug from './debug/reducer';

export {
  auth,
  signup,
  debug,
};