import { StyleSheet, Dimensions } from 'react-native';

import colors from 'src/app/colors';

var {
  height,
  width
} = Dimensions.get( 'window' );

module.exports = StyleSheet.create( {
  container: {
    maxHeight: 80
  },
  touchableContainer: {
    //  height: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-start',
    maxHeight: 50
  },
  infoText: {
    color: colors.secondary,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center'
  },
  actionText: {
    color: colors.primary,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    textDecorationLine: 'underline'
  },
  underlinedActionText: {
    color: colors.primary,
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center'
  }
} );
