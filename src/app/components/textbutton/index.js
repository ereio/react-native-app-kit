import React, { Component } from 'react';

import { TouchableNativeFeedback, TouchableOpacity, Image, Text, View } from 'react-native';

// Global and Local Styling
import styles from './styles';

const EMPTY = "";
const INFO_SPACE = " ";

export default class TextButton extends Component {
  constructor( props ) {
    super( props );
    this.state = {
      infoText: this.props.infoText || EMPTY,
      actionText: this.props.actionText || EMPTY,
      containerStyle: styles.touchableContainer,
      textStyle: this.props.underlined
        ? styles.underlinedActionText
        : styles.actionText
    }

    if ( this.state.infoText !== "" ) 
      this.state.infoText += INFO_SPACE;
    }
  
  render() {
    return ( <View style={[ styles.container, this.props.style ]}>
      <TouchableOpacity
        onPress={this.props.onPress}
        style={this.state.containerStyle}>
        <Text style={styles.infoText}>{this.state.infoText}
          <Text style={this.state.textStyle}>{this.state.actionText}
          </Text>
        </Text>
      </TouchableOpacity>
    </View> );
  }
}
