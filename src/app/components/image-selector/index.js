import React, { Component } from 'react';
import { TouchableNativeFeedback, Dimensions, Image, View } from 'react-native';
import PropTypes from 'prop-types';

// Libraries
import ImageResizer from 'react-native-image-resizer';
import ImagePicker from 'react-native-image-picker';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import colors from 'src/app/colors';
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';
const { height, width } = Dimensions.get('window');

const MAX_IMG_HEIGHT = 350;
const MAX_IMG_WIDTH = 350;

const options = {
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};

class ImageSelector extends Component {
  constructor(props) {
    super(props);
    this.selectImage = this.selectImage.bind(this);
    this.resizeImage = this.resizeImage.bind(this);
  }

  selectImage() {
    ImagePicker.showImagePicker(options, (source) => {
      if(source.didCancel) {
        console.log('User cancelled image picker');
      } else if(source.error) {
        console.log('ImagePicker Error: ', source.error);
      } else if(source.customButton) {
        console.log('User tapped custom button: ', source.customButton);
      } else {
        this.resizeImage(source);
      }
    });
  };

  resizeImage(source) {
    const { onImageSelect } = this.props;

    const image = {
      uri: source.uri,
      newWidth: source.width > MAX_IMG_WIDTH ?
        MAX_IMG_WIDTH : source.width,
      newHeight: source.height > MAX_IMG_HEIGHT ?
        MAX_IMG_HEIGHT : source.height,
      compressFormat: 'PNG'
    }

    ImageResizer
      .createResizedImage(image.uri, image.newWidth, image.newHeight,
        image.compressFormat, 100)
      .then((compressedImage) => {
        // response.uri is the URI of the new image that can now be displayed,
        // uploaded... response.path is the path of the new image response.name is the
        // name of the new image with the extension response.size is the size of the new
        // image
        onImageSelect(compressedImage.uri);
      })
      .catch((error) => {
        console.log("ERROR", error);
      });
  };

  renderImage(imageUri) {
    if(imageUri) {
      console.log("Loading", imageUri)
      return(
        <Image style={styles.imageCircle} source={{ uri: imageUri }}/>
      );
    } else {
      return(
        <MaterialIcon name='account-circle' size={width * 0.35} style={{ color: colors.textPrimary }}/>
      );
    }
  };

  render() {
    const { imageUri } = this.props;
    return(
      <View style={[styles.container, this.props.style]}>
        <TouchableNativeFeedback
          onPress={this.selectImage}
          background={TouchableNativeFeedback.Ripple(colors.accent)}>
          <View style={styles.pickerContainer}>
            <View style={styles.imageContainer}>
              {this.renderImage(imageUri)}
            </View>
          </View>
        </TouchableNativeFeedback>
      </View>
    );
  }
}

ImageSelector.propTypes = {
  onImageSelect: PropTypes.func.isRequired,
  imageUri: PropTypes.string.isRequired,
};

export default ImageSelector;