import { StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
  },
  pickerContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.secondary,
    borderRadius: 10,
    borderColor: colors.secondary,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 2
    }
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageCircle: {
    width: width * 0.35,
    height: width * 0.35,
    borderRadius: 100,
  },
});