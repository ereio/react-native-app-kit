import React, { Component } from 'react';
import { TouchableNativeFeedback, TouchableOpacity, Image, Text, View } from 'react-native';

// Lib Imports
import { MKTextField } from 'react-native-material-kit';

// Global Styling
import colors from 'src/app/colors';

// Local Styling
import styles from './styles';

const FloatingLabel = MKTextField
  .textfield()
  .withPlaceholderTextColor(colors.textAlternate)
  .withTintColor(colors.secondary)
  .withHighlightColor(colors.secondary)
  .withSelectionColor(colors.primary)
  .withStyle(styles.floatingLabel)
  .withTextInputStyle(styles.formInput)
  .withFloatingLabelFont({ fontSize: 18 })
  .withUnderlineSize(1)
  .build();

export default class MaterialMultiTextField extends Component {
  render() {
    return(
      <View style={[ styles.container, this.props.style ]}>
      <FloatingLabel
        multiline={true}
        numberOfLines={4}
        {...this.props}/>
    </View>
    );
  }
}