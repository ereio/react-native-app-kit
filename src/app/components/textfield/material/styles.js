import { StyleSheet, Dimensions } from 'react-native';

let { height, width } = Dimensions.get( 'window' );

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create( {
  container: {
    marginTop: -10,
    marginHorizontal: 12
  },
  floatingLabel: {
    height: 60, // have to do it on iOS
    marginTop: 10
  },
  formInput: {
    fontSize: 20,
    color: colors.accent
  }
} );
