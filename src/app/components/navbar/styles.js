import { StyleSheet, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';
import dimens from 'src/app/dimens';

module.exports = StyleSheet.create({
  main: {
    backgroundColor: colors.secondary
  },
  // OVERRIDES DEFAULT REACT NAVIGATION PLATFORM STYLING
  transparent: {
    backgroundColor: 'transparent',
    shadowColor: 'transparent',
    shadowOpacity: 0,
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
    },
    elevation: 0,
  },
  shaded: {
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    marginTop: width * dimens.DEFAULT_RATIO_SMALL,
    marginHorizontal: width * dimens.DEFAULT_RATIO_SMALL,
    shadowColor: 'transparent',
    shadowOpacity: 0,
    shadowRadius: 0,
    shadowOffset: {
      height: 0,
    },
    elevation: 0,
  },
  title: {
    fontSize: 28,
    fontWeight: '600',
    color: colors.textSecondary,
    borderColor: colors.primary
  },
  userTitle: {
    fontSize: 28,
    fontWeight: '600',
    color: colors.textPrimary,
    borderColor: colors.primary
  },
  detailTitle: {
    fontSize: 24,
    fontWeight: '600',
    marginLeft: 0,
    color: colors.textPrimary
  },
  tint: {
    color: colors.primary
  },
  navHeaderRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginHorizontal: 10
  },
  navIcons: {
    color: colors.primary,
    marginHorizontal: 5
  },
  indicatorBar: {
    color: colors.primary
  },
  settingsIcon: {
    color: colors.textSecondary,
    marginRight: width * dimens.DEFAULT_RATIO,
  }
});