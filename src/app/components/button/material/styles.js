import { StyleSheet, Dimensions } from 'react-native';

const { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create({
  // ROOT STYLE
  container: {
    flex: 1,
    marginHorizontal: 10,
    maxHeight: 50
  },
  button: {
    borderRadius: 2,
    shadowRadius: 1,
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowOpacity: 0.7,
    shadowColor: 'black',
    elevation: 4,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  // TEXT STYLES
  text: {
    color: colors.white,
    fontSize: 18,
    fontWeight: '400',
    padding: 8
  }
});