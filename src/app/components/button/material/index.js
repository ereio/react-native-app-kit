import React, { Component } from 'react';
import { Platform, Text, View } from 'react-native';

// Lib Imports
import { MKButton } from 'react-native-material-kit';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import dimens from 'src/app/dimens';
import colors from 'src/app/colors';

// Local Styling
import styles from './styles';

export default class MaterialButton extends Component {
  render() {
    return(
      <View style={[ styles.container, this.props.style ]}>
      <MKButton
        style={styles.button}
        raised={true}
        backgroundColor={colors.primary}
        {...this.props}>
        <Text pointerEvents="none" style={styles.text}>
          {this.props.text}
        </Text>
      </MKButton>
    </View>
    );
  }
}