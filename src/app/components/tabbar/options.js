// Style Imports
import dimens from 'src/app/dimens';
import colors from 'src/app/colors';

module.exports = {
  activeTintColor: colors.textSecondary,
  inactiveTintColor: colors.textInactive,
  showIcon: true,
  showLabel: true,
  upperCaseLabel: false,
  scrollEnabled: false,
  indicatorStyle: {
    backgroundColor: colors.primary,
    height: 0,
  },
  tabStyle: {
    height: 80,
  },
  labelStyle: {
    fontSize: 15,
    marginTop: 0,
  },
  iconStyle: {
    width: dimens.tabIcon,
    height: dimens.tabIcon,
  },
  style: {
    height: 70,
    backgroundColor: colors.primary,
  },
}