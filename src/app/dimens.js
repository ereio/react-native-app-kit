module.exports = {
  tabIcon: 42,
  loadingIconSizeRatio: 0.25,
  profileImageRatio: 0.25,
  DEFAULT_RATIO_SMALL: 0.02,
  DEFAULT_RATIO: 0.04,
  DEFAULT_RATIO_LARGE: 0.08,
}