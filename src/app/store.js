// Default Redux Imports
import { createStore, applyMiddleware, combineReducers } from 'redux';
import * as keychain from 'react-native-keychain';
import thunk from 'redux-thunk';

// Redux persist imports
import { persistStore, persistReducer, persistCombineReducers } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
import * as introReducers from 'src/app/domain/intro';
import * as userReducers from 'src/app/domain/user';

// Redux persist encryption
import createEncryptor from 'redux-persist-transform-encrypt';

const encryptor = createEncryptor({
  secretKey: "default",
  onError: function(error) {
    console.log("CREATE ENCRYPTOR ERROR", error);
  }
})

const defaultPersistConfig = {
  key: "default",
  storage: storage,
  transforms: [encryptor],
  whitelist: ["auth"],
}

const introCombinedReducers = combineReducers(introReducers);
const introPersistedReducer = persistReducer(defaultPersistConfig,
  introCombinedReducers);

const rootReducer = combineReducers({ intro: introPersistedReducer });

const store = createStore(rootReducer, applyMiddleware(thunk));
let persistor = persistStore(store);

const accessUserEncryptedStore = (key, password) => {
  const userEncryptor = createEncryptor({
    secretKey: password,
    onError: function(error) {
      console.log("CREATE ENCRYPTOR ERROR", error);
    }
  })

  const userPersistConfig = {
    key: key,
    storage: storage,
    transforms: [userEncryptor],
    timeout: 10000,
  }

  const combinedUserReducer = combineReducers(userReducers);
  const userPersistedReducer = persistReducer(userPersistConfig,
    combinedUserReducer)
  const updatedRootReducer = combineReducers({
    intro: introPersistedReducer,
    user: userPersistedReducer,
  })

  store.replaceReducer(updatedRootReducer);
  persistor.persist();
}

const lockUserEncryptedStore = () => {
  const updatedRootReducer = combineReducers({
    intro: introPersistedReducer,
  })

  persistor.flush().then(() => {
    store.replaceReducer(updatedRootReducer);
    persistor.persist();
  })
  console.log("LOCKED REPLACEMENT STORE", store.getState());
  console.log("LOCKED USER STATE REPLACEMENT PERSISTOR", persistor.getState());
}


export { store, persistor, accessUserEncryptedStore, lockUserEncryptedStore };