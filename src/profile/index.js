import React, { Component } from 'react';
import {
  Platform,
  Dimensions,
  TouchableOpacity,
  Image,
  Text,
  View
} from 'react-native';

// Redux
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// Actions
import * as authActions from 'src/app/domain/intro/auth/actions';
import * as profileActions from 'src/app/domain/user/profile/actions';
import * as collectableActions from 'src/app/domain/user/collectables/actions';

// Libs
import LinearGradient from 'react-native-linear-gradient';
import MaterialButton from 'src/app/components/button/material';
// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

// Global Styling
import navbar from 'src/app/components/navbar/styles';
import colors from 'src/app/colors';
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';
const { height, width } = Dimensions.get('window');

class Profile extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
    const { params = { onClickSettings: () => { return; } } } = state;
    return {
      headerTransparent: true,
      headerStyle: navbar.shaded,
      headerPressColorAndroid: colors.secondary,
      headerTitle: 'Welcome ' + params.name + '!',
      headerRight: (
        <TouchableOpacity
          onPress={params.onClickSettings}>
          <MaterialIcon
            name="settings"
            size={dimens.tabIcon}
            style={navbar.settingsIcon}/>
        </TouchableOpacity>
      ),
      tabBarIcon: ({ tintColor }) => (
        <MaterialIcon
          name="account-circle"
          size={dimens.tabIcon}
          style={{
            color: tintColor
          }}/>
      )
    };
  };
  constructor(props) {
    super(props);

    this.onClickLogout = this.onClickLogout.bind(this);
  }

  onClickLogout() {
    const { attemptLogout } = this.props.actions;
    const { navigate } = this.props.navigation;
    attemptLogout();
    navigate("IntroViews");
  }

  componentWillMount() {
    const { navigation } = this.props;
    const { username } = this.props.profile;
    navigation.setParams({
      onClickSettings: this.onClickLogout,
      name: username
    });
  }

  renderImage(imageUri) {
    if(imageUri) {
      return(
        <Image style={styles.photoCircle} source={{ uri: imageUri }}/>
      );
    } else {
      return(
        <MaterialIcon name='account-circle' size={width * dimens.profileImageRatio} style={{ color: colors.textPrimary }}/>
      );
    }
  };

  render() {
    const { profile, collectables } = this.props;
    const { incrementCount } = this.props.actions;

    return(
      <LinearGradient
        colors={[colors.backgroundPrimary, colors.backgroundSecondary]}
        start={{x: 0.0, y: 0.25}} end={{x: 0.5, y: 1.0}}
        style={styles.container}>
          <View style={styles.cardContainer}>
            <View style={styles.headerContainer}>
              <View style={styles.photoContainer}>
                  {this.renderImage(profile.photoUri)}
              </View>
              <View style={styles.headerInfoContainer}>
                <View style={styles.statContainer}>
                  <Text style={styles.primaryText}>
                    { collectables.cones.length }
                  </Text>
                  <Text style={styles.subheaderText}>
                    {"number of\n cones"}
                  </Text>
                </View>
                <View style={styles.statContainer}>
                  <Text style={styles.primaryText}>
                    { collectables.count }
                  </Text>
                  <Text style={styles.subheaderText}>
                    {"total\ncollectables"}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.personalInfoContainer}>
              <View style={styles.identificationContainer}>
                <Text style={styles.primaryInfoText}>
                  { profile.username }
                </Text>
                <Text style={styles.secondaryInfoText}>
                  { profile.email }
                </Text>
              </View>
              <View style={styles.aboutContainer}>
                <Text style={styles.primaryInfoTextSmall}>
                  { profile.about }
                </Text>
            </View>
            </View>
            <View style={styles.actionsContainer}>
              <MaterialButton text="Edit Profile" onPress={incrementCount} />
            </View>
        </View>
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.user.profile,
  collectables: state.user.collectables
});

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ ...profileActions,
      ...authActions,
      ...collectableActions
    }, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);