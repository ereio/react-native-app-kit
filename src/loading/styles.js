import { Platform, StyleSheet, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');

// Style Imports
import colors from 'src/app/colors';

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.default
  },
  splashIcon: {
    height: width * 0.25,
    width: width * 0.25
  }
});