import React, { Component } from 'react';
import {
  Animated,
  Dimensions,
  Easing,
  Platform,
  Text,
  View,
  Image
} from 'react-native';

// Icons
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { APP_ICON } from 'src/app/assets';

// Global Styling
import dimens from 'src/app/dimens';

// Local Styling
import styles from './styles';

let { height, width } = Dimensions.get('window');

export default class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scale: new Animated.Value(width * 0.28),
      fading: false
    }
  }

  componentDidMount() {
    const { init } = this.props;
    this.startLoadingAnimationLoop(init);
  }

  startEndLoadingAnimation() {
    const { onFinished } = this.props;
    Animated
      .sequence([
          Animated.timing(this.state.scale, {
          toValue: -10,
          duration: 500
        })
      ])
      .start(onFinished);
  }

  checkFinishedLoading(status, isLoadingComplete) {
    if(status.finished && isLoadingComplete) {
      this.startEndLoadingAnimation();
    } else {
      this.startLoadingAnimationLoop(true);
    }
  }
  startLoadingAnimationLoop(isLoading, isEnding) {
    const { onFinished, isLoadingComplete } = this.props;
    if(isLoading) {
      Animated
        .sequence([
            Animated.timing(this.state.scale, {
            toValue: width * 0.18,
            easing: Easing.back(),
            duration: 1500
          }),
            Animated.timing(this.state.scale, {
            toValue: width * 0.28,
            easing: Easing.back(),
            duration: 1500
          }),
        ])
        .start((status) => this.checkFinishedLoading(status,
          isLoadingComplete));
    }
  }

  render() {
    return(
      <View style={styles.container}>
        <Animated.Image
          style={{
            height: this.state.scale,
            width: this.state.scale,
            resizeMode: 'contain'
          }}
          source={APP_ICON}/>
    </View>
    );
  }
}