import React, { Component } from 'react';
import { AppRegistry, StatusBar, StyleSheet, Text, View } from 'react-native';
import { YellowBox } from 'react-native';

// Nav Imports
import { StackNavigator, TabNavigator, SwitchNavigator } from 'react-navigation';

// State Management Imports
import { store, persistor, accessUserEncryptedStore } from 'src/app/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'

// Tab View Imports
import Primary from 'src/primary';
import Secondary from 'src/secondary';
import Auxiliary from 'src/auxiliary';
import Profile from 'src/profile';

// Data Views
import Details from 'src/details';

// Intro Views
import Login from 'src/login';
import Signup from 'src/signup';
import Intro from 'src/intro';
import Debug from 'src/debug';

// Loading Views
import Splash from 'src/splash';

// Style Imports
import * as tabbar from 'src/app/components/tabbar';
import navbar from 'src/app/components/navbar/styles';
import colors from 'src/app/colors';

// TODO: ASAP REMOVE
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated',
  'Module RCTImageLoader']);

const DebugView = TabNavigator({
  Debug: {
    screen: Debug
  },
  Primary: {
    screen: Primary
  },
  Secondary: {
    screen: Secondary
  },
  Auxiliary: {
    screen: Auxiliary
  },
}, {
  swipeEnabled: false,
  animationEnabled: false,
  backBehavior: 'none',
  tabBarPosition: 'bottom',
  tabBarOptions: tabbar.options,
  headerMode: 'screen',
  navigationOptions: {
    headerStyle: navbar.main,
    headerTitleStyle: navbar.title,
    headerTintColor: colors.primary,
    headerPressColorAndroid: colors.secondary
  }
});

const TabView = TabNavigator({
  Primary: {
    screen: Primary
  },
  Secondary: {
    screen: Secondary
  },
  Auxiliary: {
    screen: Auxiliary
  },
  Profile: {
    screen: Profile
  },
}, {
  swipeEnabled: false,
  animationEnabled: false,
  backBehavior: 'none',
  tabBarPosition: 'bottom',
  tabBarOptions: tabbar.options,
  headerMode: 'screen',
  navigationOptions: {
    headerStyle: navbar.main,
    headerTitleStyle: navbar.title,
    headerTintColor: colors.primary,
    headerPressColorAndroid: colors.secondary
  }
});

const AppView = StackNavigator({
  TabView: {
    screen: TabView
  },
  Details: {
    screen: Details
  }
}, {});

const IntroView = StackNavigator({
  Intro: {
    screen: Intro
  },
  Login: {
    screen: Login
  },
  Signup: {
    screen: Signup
  }
}, {
  headerMode: 'screen',
  navigationOptions: {
    headerStyle: navbar.mainEmpty,
    headerTransparent: true,
    headerPressColorAndroid: colors.secondary
  }
});

const MainView = SwitchNavigator({
  Splash: {
    screen: Splash
  },
  AppView: {
    screen: AppView
  },
  IntroViews: {
    screen: IntroView
  }
}, {
  navigationOptions: {
    headerStyle: navbar.main,
    headerTitleStyle: navbar.title,
    headerTintColor: colors.primary,
    headerPressColorAndroid: colors.secondary
  }
})

const onBeforeLift = () => {
  // TODO: Add splash screen to background check login info here
  // take some action before the gate lifts
}

export class App extends Component {
  componentDidMount() {
    //TODO: for testing! i like these persistor actions a lot
    // persistor.purge();
  }

  render() {
    return(
      <Provider store={store}>
        <PersistGate loading={null} onBeforeLift={onBeforeLift} persistor={persistor}>
          <View style={{ flex: 1 }}>
            <StatusBar barStyle="light-content" backgroundColor={colors.secondary}/>
            <MainView />
          </View>
      </PersistGate>
    </Provider>
    );
  }
}

AppRegistry.registerComponent('Starter', () => App);