# React Native App Kit : WIP

After building out a react native application over the course of 9 months, I realized I made a lot of mistakes.
Not only did I forgo a lot of standards, but I didn't know what I didn't know when it came to implementations.
I realized quickly that it can be difficult to find examples of more intricate but common app features,
so I decided to build out a complete example application. There's no connected services, but it treats the store as if
it has already pulled data from a service. Let me know if you find something missing that you consider essential!
This is a living project, and after the base of the app has been implemented, I'll be updating it as things change to react native.

That said, I'm already behind on react-navigation, so bare with me!

Includes lots of examples of:
  - [react-navigation(v1)](https://v1.reactnavigation.org/docs/getting-started.html)
  - [react-redux](https://github.com/reduxjs/react-redux)
  - [redux-thunk](https://github.com/reduxjs/redux-thunk)
  - [redux-persist](https://github.com/rt2zz/redux-persist)
  - [redux-persist-transform-encrypt](https://github.com/maxdeviant/redux-persist-transform-encrypt)
  - [react-native-bcrypt](https://www.npmjs.com/package/react-native-bcrypt) for hashing passwords
    - using [seedrandom](https://www.npmjs.com/package/seedrandom) for PRNG
  - [file structure](http://www.javapractices.com/topic/TopicAction.do?Id=205) / [airbnb styleguide adherence](https://github.com/airbnb/javascript)
  - and several awesome material UI libraries :)
  - rehydrating encrypted stores based on user
    - [dynamic store management](https://stackoverflow.com/questions/32968016/how-to-dynamically-load-reducers-for-code-splitting-in-a-redux-application)
    - [code splitting](http://nicolasgallagher.com/redux-modules-and-code-splitting/)
    - [basically what i need](https://github.com/rt2zz/redux-persist/issues/623)
The app's primary screens a bottom nav bar consisting of 4 panels:
  - primary
  - secondary
  - auxiliary
  - profile

Additional screens include:
  - details
  - login
  - signup

I'll try to keep this updated with every new react native version, but let me know if I'm missing something!

## Log
  - [hot swapping stores 1](https://github.com/reduxjs/redux/pull/667)
  - [hot swapping stores 2](https://github.com/reduxjs/react-redux/pull/80)
  - [combining reducers](https://redux.js.org/api-reference/combinereducers)
## Small Meaningful React Native / Native References:
  - Always remember features usually require permissions, such as selecting photos
  ```
  Android - AndroidManifest.xml
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
  iOS -
  ```
  - Absolute Path References
    - package.json in the root directory with {name: "src"}
    - npm install babel-root-slash-import will allow you to reference "/src" (indeterminate with Metro Builder)
  - [Recent Apps Overview Color Change](https://stackoverflow.com/questions/44978530/react-native-how-to-change-recent-apps-background-title-color-on-android#45093166)
  - Hide header for specific pages
    - headerMode: "none" doesn't work as an override
    - [static navigationOptions = {header:null} does within the component](https://github.com/react-navigation/react-navigation/issues/53)
  - Allow keyboard to shift whole screen up for input (Android Only)
      ```
      <activity
        ...
        android:windowSoftInputMode="adjustPan">
      ```
  - Hide React Navigation header [for individual screens](https://github.com/react-navigation/react-navigation/issues/293)
  -
## Backlog:
  - app icon animations on signup screens when section is completed
  - add app icon to iOS
  - change button to checkmark and fill/transition when page is considered acceptable
  - move signupLoadingPage to StackNavigation of IntroViews - can't be a view pager page
  - subscribe to react navigation events to prevent transitions more holistically - right now just using isFocused()

## Opinions
  - [Mixins are awful](https://reactjs.org/blog/2016/07/13/mixins-considered-harmful.html)
